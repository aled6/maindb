﻿import-module vmware.vimautomation.core
Function Connect_Environment(){
$script:vc_cred = get-credential I2-148@vsphere.local

$script:vcenter_serv = Connect-viserver vcenter -Credential $vc_cred

}

Connect_Environment

#Html et CSS code
$header =@"
<head>

    <style type"text/css">
    <!-
        body {
        font-family: Veranda, Geneva, Arial, Helvetica, sans-serif;
        background-color: darkblue;
        }

        #report {width: 835px; }
        table{
        border-collapse: collapse;
        border: none;
        front: 10pt Verdana, Geneva, Arial, Helvetica, sans-serif;
        color: black;
        margin-bottom: 10px;
        }

        table td{
        font-size: 12px;
        padding-left: 0px;
        padding-right: 20px;
        text-align: left;
        }

        table th {
        font-size: 12px;
        padding-left: 0px;
        padding-right: 20px;
        text-align: left;
        }
        
        table.list{ float: left;}

        table.list td:nth-child(1){
        font-weight: bold;
        border-right: 1px grey solid:
        text-align: right;
        }

        table.list td:nth-child(2){padding-lef: 7px; }
        table tr:nth-child(even) td;nth-child(even){background: #BBBBBB;}
        table tr:nth-child(odd) td;nth-child(odd){background: #F2F2F2;}
        table tr:nth-child(even) td;nth-child(odd){background: #DDDDDD;}
        table tr:nth-child(odd) td;nth-child(even){background: #E5E5E5;}
        div.column {width: 320px: float: left;}
        div.second{ margin-left: 30px;}
        table{margin-left: 20px;}
        ->

    </style>

</head>
        
"@

#Repo de stockage de fichiers generes
function Global_Var(){   $Global:xmlpath =  "D:\Logiciel\Vmware powercli\Reports\WebPage\XML-STORE\"
                         $Global:htmlpath  = "D:\Logiciel\Vmware powercli\Reports\WebPage\HTML-STORE\"

                    }


Global_Var
#######Stocker########################

#Reference dans partie 2
$xmlname= Get-Date + $xmlpath + "VMS.xml"  
$htmlname= Get-Date -format dd.MM.yyyy-hh.mm $htmlpath + "VMS.html"
$info = get-vm *aled*
$info | Export-Clixml $xmlname
$info |convertto-html -Head $header | out-file $htmlname

$log = "Generation des reports en cours..."

######################################
#Reference dans Partie 3

#Nom et chemin du fichier genere

$xmlname= $xmlpath + "VMDetails.xml"
$htmlname= $htmlpath + "VMDetails.html"

#stocker get-vm information et sauvegarder l'affichage dans html/xml formats
$info = get-vm -Name *aled*  | select *aled*
$info | Export-Clixml $xmlname
$info |convertto-html -Head $header | out-file $htmlname

#stocker get-view information et sauvegarder l'affichage dans html/xml formats
$xmlname= $xmlpath + "VM_ViewDetails.xml"
$htmlname= $htmlpath + "VM_ViewDetails.html"
$vm = Get-View  –ViewType VirtualMachine -Filter @{"name"= "aled"}
$vm | Export-Clixml $xmlname
$vm |convertto-html -Head $header | out-file $htmlname



########################################################################
#Datastore
$xmlname= $xmlpath + "DatastoreInfo.xml"
$htmlname= $htmlpath + "DatastoreInfo.html"
$Datastores = Get-Datastore -server $Vcenter | Select *
$Datastores | Export-Clixml $xmlname
$Datastores |convertto-html -Head $header | out-file $htmlname

$xmlname= $xmlpath + "DataStoreExtension.xml"
$htmlname= $htmlpath + "DataStoreExtension.html"
$Data_extend = $datastores.extensiondata
$Data_extend | Export-Clixml $xmlname
$Data_extend |convertto-html -Head $header | out-file $htmlname