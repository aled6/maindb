﻿######Script to generate information about vm#################
#
#
#Utilisez -server $Vcenter_server devant chaque commande Get-XXX si besoin
#
#
#
################################################################
#Import Core Module  
import-module vmware.vimautomation.core

#Declaration de variables globales

#VCenter identifiants
$VCenter_Cred = get-credential I2-148@vsphere.local

$Vcenter_Server = Connect-VIServer vcenter -Credential $VCenter_Cred


#Partie 2
Get-VM *aled* 


#Partie 3

#Return the rest of the available properties form the query
$vm = Get-VM   #GuestId

#Where are all of the settings and values?
$VM = Get-View  –ViewType VirtualMachine –filter @{“name”=“aled”}
$VM.summary.guest.toolsstatus
$VM.summary.config.numCpu



##########################################################################
#
#
#
# La suite concerne des informations generales, datasource, cluster...
##Datastore
#
#
#Mod 6

get-datastore 

Get-Datastore -server $Vcenter_server | Select * 

$Datastores = Get-Datastore -server $Vcenter_server | Select * # |Out-GridView

$DrillDown = $Datastores | where {($_.FreeSpaceGB -gt 200)}
$DrillDown |ft -auto
$DataSpecific = $Datastores | where{($_.name -eq "ESXI-1-R1-3")}
$Dataspecific.ExtensionData
$Dataspecific.ExtensionData.overallstatus

################################################################
