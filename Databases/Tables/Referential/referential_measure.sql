-- Table: public.referential_measure

-- DROP TABLE public.referential_measure;

CREATE TABLE public.referential_measure
(
    ref_measure integer NOT NULL DEFAULT nextval('referential_measure_ref_measure_seq'::regclass),
    level_measure character varying(255) COLLATE pg_catalog."default",
    type_measure character varying(255) COLLATE pg_catalog."default",
    up_date timestamp without time zone,
    wip_date timestamp without time zone,
    CONSTRAINT referential_measure_pkey PRIMARY KEY (ref_measure)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.referential_measure
    OWNER to postgres;