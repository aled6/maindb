-- Table: public.referential_position

-- DROP TABLE public.referential_position;

CREATE TABLE public.referential_position
(
    ref_position integer NOT NULL DEFAULT nextval('referential_position_ref_position_seq'::regclass),
    emplacement character varying(255) COLLATE pg_catalog."default",
    name character varying(255) COLLATE pg_catalog."default",
    surface double precision,
    up_date timestamp without time zone,
    wip_date timestamp without time zone,
    CONSTRAINT referential_position_pkey PRIMARY KEY (ref_position)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.referential_position
    OWNER to postgres;