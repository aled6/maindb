-- Table: public.referential_bracelet

-- DROP TABLE public.referential_bracelet;

CREATE TABLE public.referential_bracelet
(
    id integer NOT NULL DEFAULT nextval('referential_bracelet_id_seq'::regclass),
    brand character varying(255) COLLATE pg_catalog."default",
    name character varying(255) COLLATE pg_catalog."default",
    is_gps boolean,
    is_waterproof boolean,
    is_cardiacfrequency boolean,
    up_date timestamp without time zone,
    wip_date timestamp without time zone,
    CONSTRAINT referential_bracelet_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.referential_bracelet
    OWNER to postgres;