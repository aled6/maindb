-- Table: public.measure

-- DROP TABLE public.measure;

CREATE TABLE public.measure
(
    id_measure bigint NOT NULL DEFAULT nextval('measure_id_measure_seq'::regclass),
    count_podytemperature character varying(255) COLLATE pg_catalog."default",
    count_cardiacfrequency character varying(255) COLLATE pg_catalog."default",
    count_distance character varying(255) COLLATE pg_catalog."default",
    count_expiratoryflowrate character varying(255) COLLATE pg_catalog."default",
    count_oxygensaturation character varying(255) COLLATE pg_catalog."default",
    count_paths character varying(255) COLLATE pg_catalog."default",
    count_respiratoryrate character varying(255) COLLATE pg_catalog."default",
    count_sleep character varying(255) COLLATE pg_catalog."default",
    count_waterresistance character varying(255) COLLATE pg_catalog."default",
    CONSTRAINT measure_pkey PRIMARY KEY (id_measure)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.measure
    OWNER to postgres;